package com.nmeo.controllers;

import com.nmeo.exceptions.PokemonAlreadyExistsException;
import com.nmeo.models.Pokemon;
import com.nmeo.services.PokedexService;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;

public class PokemonCreateController implements Handler {
    private final PokedexService pokedexService;

    public PokemonCreateController(PokedexService pokedexService) {
        this.pokedexService = pokedexService;
    }

    @Override
    public void handle(@NotNull Context ctx) throws Exception {
        Pokemon pokemon = ctx.bodyAsClass(Pokemon.class);

        try {
            pokedexService.createPokemon(pokemon);
        } catch (PokemonAlreadyExistsException e) {
            ctx.status(400);
            return;
        }

        ctx.status(200);
    }
}
