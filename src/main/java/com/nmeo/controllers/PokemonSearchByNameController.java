package com.nmeo.controllers;

import com.nmeo.dto.SearchResult;
import com.nmeo.models.Pokemon;
import com.nmeo.services.PokedexService;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PokemonSearchByNameController implements Handler {
    private final PokedexService pokedexService;

    public PokemonSearchByNameController(PokedexService pokedexService) {
        this.pokedexService = pokedexService;
    }

    @Override
    public void handle(@NotNull Context ctx) throws Exception {
        String nameToSearch = ctx.queryParam("name");
        List<Pokemon> results = pokedexService.searchPokemonByName(nameToSearch);

        ctx.status(200);
        ctx.json(new SearchResult(results));
    }
}
