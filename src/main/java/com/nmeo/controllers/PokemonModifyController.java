package com.nmeo.controllers;

import com.nmeo.dto.ModifyPokemonRequest;
import com.nmeo.exceptions.PokemonNotFoundException;
import com.nmeo.services.PokedexService;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;

public class PokemonModifyController implements Handler {
    private final PokedexService pokedexService;

    public PokemonModifyController(PokedexService pokedexService) {
        this.pokedexService = pokedexService;
    }

    @Override
    public void handle(@NotNull Context ctx) throws Exception {
        ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);

        try {
            pokedexService.modifyPokemon(request);
        } catch (PokemonNotFoundException e) {
            ctx.status(404);
            return;
        }

        ctx.status(200);
    }
}
