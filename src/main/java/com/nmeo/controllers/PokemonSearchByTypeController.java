package com.nmeo.controllers;

import com.nmeo.dto.SearchResult;
import com.nmeo.models.Elements;
import com.nmeo.models.Pokemon;
import com.nmeo.services.PokedexService;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PokemonSearchByTypeController implements Handler {
    private PokedexService pokedexService;

    public PokemonSearchByTypeController(PokedexService pokedexService) {
        this.pokedexService = pokedexService;
    }

    @Override
    public void handle(@NotNull Context ctx) throws Exception {
        String typeToSearch = ctx.queryParam("type");
        List<Pokemon> results;

        try {
            results = pokedexService.searchPokemonByType(Elements.valueOf(typeToSearch));
        } catch (IllegalArgumentException e) {
            ctx.status(400);
            return;
        }

        ctx.status(200);
        ctx.json(new SearchResult(results));
    }
}
