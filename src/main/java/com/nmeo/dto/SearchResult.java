package com.nmeo.dto;

import com.nmeo.models.Pokemon;

import java.util.List;

public class SearchResult {
    public List<Pokemon> result;

    public SearchResult(List<Pokemon> result) {
        this.result = result;
    }
}
