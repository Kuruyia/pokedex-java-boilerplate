package com.nmeo.dto;

import com.nmeo.models.Elements;
import com.nmeo.models.Power;

import java.util.List;

public class ModifyPokemonRequest {
    public String pokemonName;
    public Elements type;
    public Integer lifePoints;
    public List<Power> powers;
}
