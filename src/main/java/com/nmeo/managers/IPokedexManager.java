package com.nmeo.managers;

import com.nmeo.exceptions.PokemonAlreadyExistsException;
import com.nmeo.exceptions.PokemonNotFoundException;
import com.nmeo.models.Elements;
import com.nmeo.models.Pokemon;
import com.nmeo.models.Power;

import java.util.List;

public interface IPokedexManager {
    void addPokemon(Pokemon pokemon) throws PokemonAlreadyExistsException;

    List<Pokemon> searchPokemonByName(String query);

    List<Pokemon> searchPokemonByType(Elements query);

    void updatePokemon(String pokemonName, Elements newType, Integer newLifePoints, List<Power> addedPowers) throws PokemonNotFoundException;
}
