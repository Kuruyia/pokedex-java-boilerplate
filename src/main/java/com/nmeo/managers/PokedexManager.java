package com.nmeo.managers;

import com.nmeo.exceptions.PokemonAlreadyExistsException;
import com.nmeo.exceptions.PokemonNotFoundException;
import com.nmeo.models.Elements;
import com.nmeo.models.Pokemon;
import com.nmeo.models.Power;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

public class PokedexManager implements IPokedexManager {
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public LinkedHashMap<String, Pokemon> pokemons = new LinkedHashMap<>();

    @Override
    public void addPokemon(Pokemon pokemon) throws PokemonAlreadyExistsException {
        readWriteLock.writeLock().lock();
        Pokemon addedPokemon = pokemons.putIfAbsent(pokemon.pokemonName, pokemon);
        readWriteLock.writeLock().unlock();

        if (addedPokemon != null) {
            throw new PokemonAlreadyExistsException("The Pokemon is already registered");
        }
    }

    @Override
    public List<Pokemon> searchPokemonByName(String query) {
        readWriteLock.readLock().lock();
        List<Pokemon> result = pokemons.values()
                .stream()
                .filter(v -> v.pokemonName.contains(query))
                .collect(Collectors.toList());

        readWriteLock.readLock().unlock();
        return result;
    }

    @Override
    public List<Pokemon> searchPokemonByType(Elements query) {
        readWriteLock.readLock().lock();
        List<Pokemon> result = pokemons.values()
                .stream()
                .filter(v -> v.type.equals(query))
                .collect(Collectors.toList());

        readWriteLock.readLock().unlock();
        return result;
    }

    @Override
    public void updatePokemon(String pokemonName, Elements newType, Integer newLifePoints, List<Power> addedPowers) throws PokemonNotFoundException {
        readWriteLock.writeLock().lock();
        Pokemon pokemon = pokemons.get(pokemonName);

        if (pokemon == null) {
            throw new PokemonNotFoundException("The Pokemon is not registered");
        }

        if (newType != null) {
            pokemon.type = newType;
        }

        if (newLifePoints != null) {
            pokemon.lifePoints = newLifePoints;
        }

        if (addedPowers != null) {
            // Because this is a Set, already-existing powers will not be modified by this operation
            pokemon.powers.addAll(addedPowers);
        }

        readWriteLock.writeLock().unlock();
    }
}
