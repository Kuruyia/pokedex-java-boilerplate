package com.nmeo.exceptions;

public class PokemonAlreadyExistsException extends Exception {
    public PokemonAlreadyExistsException(String message) {
        super(message);
    }
}