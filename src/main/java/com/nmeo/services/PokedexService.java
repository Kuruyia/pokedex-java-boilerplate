package com.nmeo.services;

import com.nmeo.dto.ModifyPokemonRequest;
import com.nmeo.exceptions.PokemonAlreadyExistsException;
import com.nmeo.exceptions.PokemonNotFoundException;
import com.nmeo.managers.IPokedexManager;
import com.nmeo.models.Elements;
import com.nmeo.models.Pokemon;

import java.util.List;

public class PokedexService {
    private final IPokedexManager pokedexManager;

    public PokedexService(IPokedexManager pokedexManager) {
        this.pokedexManager = pokedexManager;
    }

    public void createPokemon(Pokemon pokemon) throws PokemonAlreadyExistsException {
        pokedexManager.addPokemon(pokemon);
    }

    public List<Pokemon> searchPokemonByName(String query) {
        return pokedexManager.searchPokemonByName(query);
    }

    public List<Pokemon> searchPokemonByType(Elements query) {
        return pokedexManager.searchPokemonByType(query);
    }

    public void modifyPokemon(ModifyPokemonRequest request) throws PokemonNotFoundException {
        pokedexManager.updatePokemon(request.pokemonName, request.type, request.lifePoints, request.powers);
    }
}
