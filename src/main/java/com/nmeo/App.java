package com.nmeo;

import com.nmeo.controllers.PokemonCreateController;
import com.nmeo.controllers.PokemonModifyController;
import com.nmeo.controllers.PokemonSearchByNameController;
import com.nmeo.controllers.PokemonSearchByTypeController;
import com.nmeo.managers.PokedexManager;
import com.nmeo.services.PokedexService;
import io.javalin.Javalin;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());

    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");

        int port = System.getenv("SERVER_PORT") != null ? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;

        PokedexManager pokedexManager = new PokedexManager();
        PokedexService pokedexService = new PokedexService(pokedexManager);

        Javalin.create()
                .get("/api/status", ctx -> {
                    logger.debug("Status handler triggered", ctx);
                    ctx.status(200);
                })
                .post("/api/create", new PokemonCreateController(pokedexService))
                .get("/api/searchByName", new PokemonSearchByNameController(pokedexService))
                .get("/api/searchByType", new PokemonSearchByTypeController(pokedexService))
                .post("/api/modify", new PokemonModifyController(pokedexService))
                .start(port);
    }
}