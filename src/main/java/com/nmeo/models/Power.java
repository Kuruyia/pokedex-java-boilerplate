package com.nmeo.models;

public class Power {
    public String powerName;
    public Elements damageType;
    public int damage;

    @Override
    public boolean equals(Object o) {
        if (o instanceof Power) {
            return powerName.equals(((Power) o).powerName);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return powerName.hashCode();
    }
}
