package com.nmeo.models;

import java.util.LinkedHashSet;

public class Pokemon {
    public String pokemonName;
    public Elements type;
    public int lifePoints;
    public LinkedHashSet<Power> powers;

    @Override
    public boolean equals(Object o) {
        if (o instanceof Pokemon) {
            return pokemonName.equals(((Pokemon) o).pokemonName);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return pokemonName.hashCode();
    }
}
